module gitlab.com/mainmast/microservices/cm-http.git

require (
	github.com/buaazp/fasthttprouter v0.1.1
	github.com/valyala/fasthttp v1.5.0
)

go 1.13
